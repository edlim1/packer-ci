
data "amazon-ami" "amazon-linux" {
  filters = {
    virtualization-type = "hvm"
    name                = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
    root-device-type    = "ebs"
  }
  owners      = ["amazon"]
  most_recent = true
}
