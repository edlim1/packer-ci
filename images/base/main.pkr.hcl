
locals {
  name_prefix = "prisma-base"
}

source "amazon-ebs" "amazon-linux" {
  ami_name      = "${local.name_prefix}-amazon-linux_{{timestamp}}"
  source_ami    = "${data.amazon-ami.amazon-linux.id}"
  ssh_username  = "${var.ssh_username}"
  ssh_pty       = true
  ssh_interface = "public_ip"
  communicator  = "ssh"
  region        = "${var.region}"
  instance_type = "${var.instance_type}"
  spot_price    = "auto"

  tags = {
    Name          = "${local.name_prefix}-amazon-linux_{{timestamp}}"
    OS_Version    = "Amazon Linux 2"
    Base_AMI_Name = "{{ .SourceAMIName }}"
    Extra         = "{{ .SourceAMITags.TagName }}"
  }
}

build {
  sources = [
    "source.amazon-ebs.amazon-linux"
  ]

  provisioner "shell" {
    pause_before = "5s"
    inline = [
      "sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm",
      "sudo systemctl enable amazon-ssm-agent",
      "sudo yum install -y jq"
    ]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
    custom_data = {
      source_ami_name = "${build.SourceAMIName}"
    }
  }
}
