
variable "region" {
  description = "Region image is built"
  default     = "us-west-2"
}

variable "ssh_username" {
  default = "ec2-user"
}

variable "instance_type" {
  description = "Instance type to build"
  default     = "t3.large"
}

variable "ami_regions" {
  description = "List of region to copy AMI to"
  type        = list(string)
  default     = ["us-west-2", "us-east-1"]
}
